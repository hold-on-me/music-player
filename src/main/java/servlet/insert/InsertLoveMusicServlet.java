package servlet.insert;

import dao.LoveMusicDao;
import dao.MusicDao;
import entity.Music;
import entity.User;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created with IntelliJ IDEA.
 * Description:用户将音乐添加至自己的喜欢列表
 * User: ZhangYufei
 * Date: 2021-07-27
 * Time: 21:01
 */
@WebServlet("/insertLoveMusic")
public class InsertLoveMusicServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.setCharacterEncoding("utf-8");
        resp.setContentType("application/json;charset=utf-8");

        //0. 判断当前是否登录
        User user = (User) req.getSession().getAttribute("user");
        if (user == null) {
            //System.out.println("当前没有登录，不能添加喜欢的音乐");
            resp.sendRedirect("login");
            return;
        }

        //1. 从req中获取需要添加的音乐和当前用户
        String musicId = req.getParameter("mid");

        //2. 判断用户是否传入参数 musicId
        if(musicId == null){
            //System.out.println("没有传入参数mid");
            req.getSession().setAttribute("delMusic","歌曲不存在");
            resp.sendRedirect("musicPage");
            return;
        }
        int mid = Integer.parseInt(musicId);

        //3. 判断传入的musicId在音乐列表中是否存在
        MusicDao musicDao = new MusicDao();
        Music music = musicDao.findMusicById(mid);
        if (music == null || "".equals(musicId)) {
            //System.out.println("传入的musicId不存在");
            req.getSession().setAttribute("delMusic","歌曲不存在");
            resp.sendRedirect("musicPage");
            return;
        }

        //4. 此时表示用户登录，同时获取到了需要添加至喜欢列表的mid
        LoveMusicDao loveMusicDao = new LoveMusicDao();
        //判断该音乐是否已经被添加至我喜欢的列表中
        if(!loveMusicDao.findLoveMusicByMusicIdAndUserId(user.getId(),mid)){
            if(loveMusicDao.insert(user.getId(),mid)){
                System.out.println("将音乐添加至我喜欢的列表成功");
            }else {
                System.out.println("添加失败");
            }
        }else{
            System.out.println("该音乐已经被添加至我喜欢了，不能再次添加");
        }

        resp.sendRedirect("musicPage");

    }
}
