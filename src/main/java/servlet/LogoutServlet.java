package servlet;

import entity.User;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

/**
 * Created with IntelliJ IDEA.
 * Description:注销操作
 * User: ZhangYufei
 * Date: 2021-07-27
 * Time: 21:15
 */
@WebServlet("/logout")
public class LogoutServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        //获取 Session
        HttpSession session = req.getSession(false);
        if (session != null) {
            session.removeAttribute("user");
        }
        //System.out.println("退出登录");
        resp.sendRedirect("index");
    }
}
