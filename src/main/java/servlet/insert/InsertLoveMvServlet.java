package servlet.insert;

import dao.LoveMVDao;
import dao.LoveMusicDao;
import dao.MVDao;
import dao.MusicDao;
import entity.MV;
import entity.Music;
import entity.User;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: ZhangYufei
 * Date: 2021-08-01
 * Time: 14:52
 */
@WebServlet("/insertLoveMv")
public class InsertLoveMvServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.setCharacterEncoding("utf-8");
        resp.setContentType("application/json;charset=utf-8");

        //0. 判断当前是否登录
        User user = (User) req.getSession().getAttribute("user");
        if (user == null) {
            resp.sendRedirect("login");
            return;
        }

        //1. 从req中获取需要添加的 mv 和当前用户
        String mvId = req.getParameter("mid");

        //2. 判断用户是否传入参数 mid
        if(mvId == null || "".equals(mvId)){
            req.getSession().setAttribute("delMv","MV不存在");
            resp.sendRedirect("mvPage");
            return;
        }
        int mid = Integer.parseInt(mvId);

        //3. 判断传入的mid在音乐列表中是否存在
        MVDao mvDao = new MVDao();
        MV mv = mvDao.findMvById(mid);
        if (mv == null) {
            req.getSession().setAttribute("delMv","MV不存在");
            resp.sendRedirect("mvPage");
            return;
        }

        //4. 此时表示用户登录，同时获取到了需要添加至喜欢列表的mid
        LoveMVDao loveMvDao = new LoveMVDao();
        //判断该音乐是否已经被添加至我喜欢的列表中
        if(!loveMvDao.findLoveMvByMvIdAndUserId(user.getId(),mid)){
            if(loveMvDao.insert(user.getId(),mid)){
                System.out.println("将 mv 添加至我喜欢的列表成功");
            }else {
                System.out.println("添加失败");
            }
        }else{
            System.out.println("该 mv 已经被添加至我喜欢了，不能再次添加");
        }

        resp.sendRedirect("mvPage");

    }
}
