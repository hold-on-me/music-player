package servlet.update;

import dao.UserDao;
import entity.User;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.WebContext;
import service.LoginService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: ZhangYufei
 * Date: 2021-08-01
 * Time: 21:56
 */
@WebServlet("/updatePassword")
public class UpdatePasswordServlet extends HttpServlet {
    public String getAndPost(HttpServletRequest req, HttpServletResponse resp,String pwd,boolean isLogin) throws ServletException, IOException {
        WebContext webContext = new WebContext(req, resp, getServletContext());
        //2) setVariable可以设置多个键值对，取决于模板代码怎么写
        // 模板中的每个 ${} 里面的内容都需要在 webContext 设定进去
        webContext.setVariable("pwd", pwd);
        webContext.setVariable("isLogin", isLogin);
        //3) 进行渲染
        TemplateEngine engine = (TemplateEngine) getServletContext().getAttribute("engine");
        return engine.process("updatePassword", webContext);
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.setCharacterEncoding("utf-8");
        //resp.setContentType("application/json;charset=utf-8");
        resp.setContentType("text/html;charset=utf-8");

        LoginService loginService = new LoginService();

        User user = loginService.checkLogin(req);
        if(user == null){
            resp.sendRedirect("login");
            return;
        }
        boolean isLogin = true;

        String html = getAndPost(req,resp,"",true);

        resp.getWriter().write(html);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.setCharacterEncoding("utf-8");
        resp.setContentType("text/html;charset=utf-8");

        //0. 判断用户是否登录
        LoginService loginService = new LoginService();
        User user = loginService.checkLogin(req);
        if(user == null){
            resp.sendRedirect("login");
            return;
        }

        //1. 获取 req 中的旧密码，新密码以及确认密码
        String oldPassword = req.getParameter("oldPassword");
        String newPassword = req.getParameter("newPassword");
        String checkPassword = req.getParameter("checkPassword");

        //2. 判断用户的旧密码是否正确
        UserDao userDao = new UserDao();
        User login = userDao.login(user.getUsername(),oldPassword);
        if (login == null) {
            String html = getAndPost(req,resp,"输入的旧密码有误",true);
            resp.getWriter().write(html);
            return;
        }

        //3. 验证两次密码是否相同
        if(!newPassword.equals(checkPassword)){
            String html = getAndPost(req,resp,"两次输入的密码不一致",true);
            resp.getWriter().write(html);
            return;

        }

        //4. 新密码不能与旧密码相同
        if(oldPassword.equals(newPassword)){
            String html = getAndPost(req,resp,"新密码不能与旧密码相同",true);
            resp.getWriter().write(html);
            return;
        }

        //5. 此时可以进行修改密码的操作
        boolean isUpdate = userDao.updatePassword(user.getId(),newPassword);
        if(isUpdate){
            resp.sendRedirect("index");
        }else{
            resp.sendRedirect("error.html");
        }
    }
}
