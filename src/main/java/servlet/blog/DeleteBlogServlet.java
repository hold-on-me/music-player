package servlet.blog;

import dao.BlogDao;
import dao.CommentDao;
import entity.Blog;
import entity.Comment;
import entity.User;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.WebContext;
import service.LoginService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: ZhangYufei
 * Date: 2021-08-02
 * Time: 16:04
 */
@WebServlet("/deleteBlog")
public class DeleteBlogServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.setCharacterEncoding("utf-8");
        resp.setContentType("application/json;charset=utf-8");

        //0. 判断用户是否登录
        LoginService login = new LoginService();
        User user = login.checkLogin(req);
        if (user == null) {
            resp.sendRedirect("login");
            return;
        }

        //1. 获取需要删除的博客id
        String blogId = req.getParameter("id");
        if(blogId == null || "".equals(blogId)){
            req.getSession().setAttribute("delBlog","需要删除的博客不存在");
            resp.sendRedirect("blogDetail?id="+blogId);
            return;
        }

        //2. 查看需要删除的博客是否是该用户发布
        BlogDao blogDao = new BlogDao();
        Blog blog = blogDao.findBlogById(Integer.parseInt(blogId));
        if(blog == null){
            req.getSession().setAttribute("delBlog","需要删除的博客不存在");
            resp.sendRedirect("blogDetail?id="+blogId);
            return;
        }
        if(!blog.getAuthor().equals(user.getUsername())){
            req.getSession().setAttribute("delBlog","你不能删除别人的博客");
            resp.sendRedirect("blogDetail?id="+blogId);
            return;
        }

        //3. 查看该篇博客是否存在评论，如果存在，则先删除评论
        CommentDao commentDao = new CommentDao();
        List<Comment> list = commentDao.findCommentByBlogId(Integer.parseInt(blogId));
        if(list != null){
            for(Comment comment : list) {
                commentDao.deleteComment(comment.getId());
            }
        }
        
        //4. 此时可以正常删除博客
        blogDao.deleteBlog(Integer.parseInt(blogId));

        //5. 删除博客后返回音乐博客主页
        resp.sendRedirect("blogList");

    }
}
