package servlet.blog;

import dao.BlogDao;
import entity.Blog;
import entity.User;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.WebContext;
import service.LoginService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: ZhangYufei
 * Date: 2021-08-02
 * Time: 16:04
 */
@WebServlet("/blogList")
public class FindBlogServlet extends HttpServlet {
    private String getAndPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        //0. 判断是否登录
        LoginService loginService = new LoginService();
        boolean isLogin = false;
        User user = loginService.checkLogin(req);
        if(user != null){
            isLogin = true;
        }

        //1. 获取关键字
        String key = req.getParameter("key");
        BlogDao blogDao = new BlogDao();
        List<Blog> list = blogDao.findSelectAllOrKey(key);

        WebContext webContext = new WebContext(req, resp, getServletContext());
        //2) setVariable可以设置多个键值对，取决于模板代码怎么写
        // 模板中的每个 ${} 里面的内容都需要在 webContext 设定进去
        webContext.setVariable("isLogin", isLogin);
        webContext.setVariable("list", list);
        //3) 进行渲染
        TemplateEngine engine = (TemplateEngine) getServletContext().getAttribute("engine");
        return engine.process("blogPage", webContext);
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.setCharacterEncoding("utf-8");
        resp.setContentType("application/json;charset=utf-8");

        String html = getAndPost(req,resp);

        resp.setContentType("text/html;charset=utf8");
        resp.getWriter().write(html);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.setCharacterEncoding("utf-8");
        resp.setContentType("application/json;charset=utf-8");

        String html = getAndPost(req,resp);

        resp.setContentType("text/html;charset=utf8");
        resp.getWriter().write(html);
    }
}
