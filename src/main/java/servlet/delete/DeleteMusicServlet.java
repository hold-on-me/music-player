package servlet.delete;

import dao.LoveMusicDao;
import dao.MusicDao;
import entity.Music;
import entity.User;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;

/**
 * Created with IntelliJ IDEA.
 * Description:如果该音乐是自己发布的话，且进行删除操作，将音乐从服务器端删除，
 * User: ZhangYufei
 * Date: 2021-07-27
 * Time: 19:06
 */
@WebServlet("/deleteMusic")
public class DeleteMusicServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.setCharacterEncoding("utf-8");
        resp.setContentType("application/json;charset=utf-8");

        //0. 判断是否登录，只能删除自己发布的音乐
        User user = (User) req.getSession().getAttribute("user");
        if (user == null) {
            //System.out.println("没有进行登录，不能进行删除操作");
            resp.sendRedirect("login");
            return;
        }

        String musicId = req.getParameter("mid");
        //1. 判断请求删除的musicId是否为空
        if (musicId == null) {
            //System.out.println("没有需要删除的musicId");
            req.getSession().setAttribute("delMusic","没有需要删除的musicId");
            resp.sendRedirect("musicPage");
            return;
        }

        MusicDao musicDao = new MusicDao();
        int mid = Integer.parseInt(musicId);
        Music music = musicDao.findMusicById(mid);
        //2. 判断数据库中是否存在该musicId
        if (music == null) {
            //System.out.println("没有需要删除的musicId");
            req.getSession().setAttribute("delMusic","没有需要删除的musicId");
            resp.sendRedirect("musicPage");
            return;
        }
        //3. 判断要删除的music是不是自己发布的
        if (music.getUserId() != user.getId()) {
            //System.out.println("你不能删除别人发布的音乐");
            req.getSession().setAttribute("delMusic","你不能删除别人发布的音乐");
            resp.sendRedirect("musicPage");
            return;
        }
        //4. 此时说明：需要删除的musicId存在，并且发布者和删除者是同一个人，可以进行删除操作
        int ret = musicDao.delete(mid);//首先删除数据库
        if (ret == 1) {
            System.out.println("数据库删除成功");
            File file = new File("/root/apache-tomcat-8.5.69/webapps/music/" + music.getUrl());//指明是哪个文件
            if (file.delete()) {//从磁盘上删除文件
                System.out.println("磁盘的音乐文件删除成功");
            } else {
                System.out.println("磁盘的音乐文件删除失败");
            }
        } else {
            System.out.println("数据库删除失败");
        }

        //5. 判断在喜欢列表中是否含有该音乐的信息，如果有，则删除
        if(musicDao.findLoveMusicById(mid)){
            LoveMusicDao loveMusicDao = new LoveMusicDao();
            int res = loveMusicDao.deleteAdmin(mid);
            if(res == 1){
                System.out.println("清除数据库中喜欢的音乐成功");
            }else{
                System.out.println("清除失败");
            }
        }

        resp.sendRedirect("musicPage");
    }
}
