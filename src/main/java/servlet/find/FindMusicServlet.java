package servlet.find;

import com.fasterxml.jackson.databind.ObjectMapper;
import dao.MusicDao;
import entity.Music;
import entity.User;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.WebContext;
import service.LoginService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * Description:用户查询音乐，不需要登录
 * User: ZhangYufei
 * Date: 2021-07-27
 * Time: 18:49
 */
@WebServlet("/musicPage")
public class FindMusicServlet extends HttpServlet {
    private String getAndPost(HttpServletRequest req, HttpServletResponse resp){

        LoginService loginService = new LoginService();
        boolean isLogin = false;
        User user = loginService.checkLogin(req);
        if(user != null){
            isLogin = true;
        }

        String musicName = req.getParameter("musicName");

        MusicDao musicDao = new MusicDao();
        List<Music> list = null;
        if(musicName != null){
            //如果musicName有值，则说明进行模糊查询
            list = musicDao.findMusicByKey(musicName);
        }else{
            //如果musicName为空值，则查询全部音乐
            list = musicDao.findMusicAll();
        }

        /*//将得到的结果返回值前端
        ObjectMapper mapper = new ObjectMapper();
        mapper.writeValue(resp.getWriter(),list);*/
        //2.构造博客页面
        //1) 通过 thymeleaf 进行渲染，渲染的时候需要定义一个"数据集合"的概念
        WebContext webContext = new WebContext(req, resp, getServletContext());
        //2) setVariable可以设置多个键值对，取决于模板代码怎么写
        // 模板中的每个 ${} 里面的内容都需要在 webContext 设定进去
        webContext.setVariable("list", list);
        webContext.setVariable("isLogin", isLogin);
        //3) 进行渲染
        TemplateEngine engine = (TemplateEngine) getServletContext().getAttribute("engine");
        return engine.process("musicPage", webContext);
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.setCharacterEncoding("utf-8");
        resp.setContentType("application/json;charset=utf-8");

        String html = getAndPost(req,resp);

        resp.setContentType("text/html;charset=utf8");
        resp.getWriter().write(html);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.setCharacterEncoding("utf-8");
        resp.setContentType("application/json;charset=utf-8");

        String html = getAndPost(req,resp);

        resp.setContentType("text/html;charset=utf8");
        resp.getWriter().write(html);
    }
}
