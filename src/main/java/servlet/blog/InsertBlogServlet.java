package servlet.blog;

import dao.BlogDao;
import entity.Blog;
import entity.User;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.WebContext;
import service.LoginService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: ZhangYufei
 * Date: 2021-08-02
 * Time: 16:03
 */
@WebServlet("/insertBlog")
public class InsertBlogServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.setCharacterEncoding("utf-8");
        resp.setContentType("application/json;charset=utf-8");

        //0. 判断用户是否登录
        LoginService loginService = new LoginService();
        boolean isLogin = false;
        User user = loginService.checkLogin(req);
        if(user != null){
            isLogin = true;
        }

        WebContext webContext = new WebContext(req, resp, getServletContext());
        //2) setVariable可以设置多个键值对，取决于模板代码怎么写
        // 模板中的每个 ${} 里面的内容都需要在 webContext 设定进去
        webContext.setVariable("isLogin", isLogin);
        //3) 进行渲染
        TemplateEngine engine = (TemplateEngine) getServletContext().getAttribute("engine");
        String html = engine.process("insertBlog", webContext);

        resp.setContentType("text/html;charset=utf8");
        resp.getWriter().write(html);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.setCharacterEncoding("utf-8");
        resp.setContentType("application/json;charset=utf-8");

        //0. 验证登录
        LoginService login = new LoginService();
        User user = login.checkLogin(req);
        if(user == null){
            resp.sendRedirect("login");
            return;
        }

        //1. 获取用户输入的标题、内容
        String title = req.getParameter("title");
        String content = req.getParameter("content");
        String category = req.getParameter("category");
        String digest = req.getParameter("digest");
        if(title == null || "".equals(title) || content == null || "".equals(content) || category == null
                || "".equals(category) || digest == null || "".equals(digest)){
            req.getSession().setAttribute("insertBlog","全部字段都需要填写");
            resp.sendRedirect("insertBlog");
            return;
        }

        //2. 向数据库中添加博客
        Blog blog = new Blog();
        blog.setTitle(title);
        blog.setDigest(digest);
        blog.setContent(content);
        blog.setAuthor(user.getUsername());
        blog.setCategory(category);
        BlogDao blogDao = new BlogDao();
        int ret = blogDao.insertBlog(blog);
        if(ret == 1){
            resp.sendRedirect("blogList");
        }
    }
}
