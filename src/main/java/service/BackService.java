package service;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created with IntelliJ IDEA.
 * Description:返回上一级
 * User: ZhangYufei
 * Date: 2021-08-09
 * Time: 22:05
 */
public class BackService {
    public void back(HttpServletResponse resp,String str) throws IOException {
        resp.sendRedirect(str);
    }
}
