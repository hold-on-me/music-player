package dao;

import entity.Blog;
import util.DBUtils;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: ZhangYufei
 * Date: 2021-08-02
 * Time: 15:34
 */
public class BlogDao {
    //添加博客
    public int insertBlog(Blog blog){
        //1. 获取数据库连接
        Connection connection = DBUtils.getConnection();
        //2. 构建 SQL 语句
        String sql = "insert into blog(title,digest,content,author,postTime,category) values(?,?,?,?,now(),?)";
        PreparedStatement statement = null;
        try {
            statement = connection.prepareStatement(sql);
            statement.setString(1,blog.getTitle());
            statement.setString(2,blog.getDigest());
            statement.setString(3,blog.getContent());
            statement.setString(4,blog.getAuthor());
            statement.setString(5,blog.getCategory());
            //3. 执行 SQL
            return statement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }finally {
            //4. 释放资源
            DBUtils.close(connection,statement,null);
        }
        return 0;
    }

    //删除博客(根据博客id)
    public int deleteBlog(int blogId){
        Connection connection = DBUtils.getConnection();
        String sql = "delete from blog where id = ?";
        PreparedStatement statement = null;
        try {
            statement = connection.prepareStatement(sql);
            statement.setInt(1,blogId);
            return statement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            DBUtils.close(connection,statement,null);
        }
        return 0;
    }

    /**
     * 显示博客
     * 1.如果 key 空，则查询全部
     * 2.如果 key 不为空，则查询博客(按照关键字)
     * @param key
     * @return
     */
    public List<Blog> findSelectAllOrKey(String key){
        Connection connection = DBUtils.getConnection();
        String sql = null;
        if(key == null || "".equals(key)){
            sql = "select * from blog order by postTime desc";
        }else{
            sql = "select * from blog where title like '%" + key + "%' order by postTime desc";
        }
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        List<Blog> list = new ArrayList<>();
        try {
            statement = connection.prepareStatement(sql);
            resultSet = statement.executeQuery();
            while (resultSet.next()){
                Blog blog = new Blog();
                blog.setId(resultSet.getInt("id"));
                blog.setTitle(resultSet.getString("title"));
                blog.setDigest(resultSet.getString("digest"));
                blog.setContent(resultSet.getString("content"));
                blog.setAuthor(resultSet.getString("author"));
                blog.setPostTime(resultSet.getTimestamp("postTime"));
                blog.setCategory(resultSet.getString("category"));
                list.add(blog);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            DBUtils.close(connection,statement,resultSet);
        }
        return list;
    }

    /**
     * 查询用户博客：
     * 1. 如果 key 为空，则查询该用户所有的博客
     * 2. 如果 key 不为空，则按照关键字查询用户的博客
     * @param key
     * @param author
     * @return
     */
    public List<Blog> findSelectUserAllOrKey(String key,String author){
        Connection connection = DBUtils.getConnection();
        String sql = null;
        if(key == null || "".equals(key)){
            sql = "select * from blog where author=? order by postTime desc";
        }else{
            sql = "select * from blog where author=? and title like '%" + key + "%' order by postTime desc";
        }
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        List<Blog> list = new ArrayList<>();
        try {
            statement = connection.prepareStatement(sql);
            statement.setString(1,author);
            resultSet = statement.executeQuery();
            while (resultSet.next()){
                Blog blog = new Blog();
                blog.setId(resultSet.getInt("id"));
                blog.setTitle(resultSet.getString("title"));
                blog.setDigest(resultSet.getString("digest"));
                blog.setContent(resultSet.getString("content"));
                blog.setAuthor(resultSet.getString("author"));
                blog.setPostTime(resultSet.getTimestamp("postTime"));
                blog.setCategory(resultSet.getString("category"));
                list.add(blog);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            DBUtils.close(connection,statement,resultSet);
        }
        return list;
    }

    //查询博客(根据博客id)
    public Blog findBlogById(int id){
        Connection connection = DBUtils.getConnection();
        String sql = "select * from blog where id=?";
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        try {
            statement = connection.prepareStatement(sql);
            statement.setInt(1,id);
            resultSet = statement.executeQuery();
            if(resultSet.next()){
                Blog blog = new Blog();
                blog.setId(resultSet.getInt("id"));
                blog.setTitle(resultSet.getString("title"));
                blog.setDigest(resultSet.getString("digest"));
                blog.setContent(resultSet.getString("content"));
                blog.setAuthor(resultSet.getString("author"));
                blog.setPostTime(resultSet.getTimestamp("postTime"));
                blog.setCategory(resultSet.getString("category"));
                return blog;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }finally {
            DBUtils.close(connection,statement,resultSet);
        }
        return null;
    }

    public static void main(String[] args) {
        BlogDao blogDao = new BlogDao();
        //1. 添加新的博客
        /*Blog blog1 = new Blog();
        blog1.setTitle("操作系统（计算机管理控制程序）");
        blog1.setDigest("介绍关于计算机操作系统的知识");
        blog1.setContent("在计算机中，操作系统是其最基本也是最为重要的基础性系统软件。从计算机用户的角度来说，计算机操作系统体现为其提供的各项服务；从程序员的角度来说，其主要是指用户登录的界面或者接口；如果从设计人员的角度来说，就是指各式各样模块和单元之间的联系。事实上，全新操作系统的设计和改良的关键工作就是对体系结构的设计，经过几十年以来的发展，计算机操作系统已经由一开始的简单控制循环体发展成为较为复杂的分布式操作系统，再加上计算机用户需求的愈发多样化，计算机操作系统已经成为既复杂而又庞大的计算机软件系统之一。");
        blog1.setAuthor("admin");
        blog1.setCategory("操作系统");
        int ret1 = blogDao.insertBlog(blog1);
        if(ret1 == 1){
            System.out.println("添加成功");
        }*/
        /*Blog blog2 = new Blog();
        blog2.setTitle("软件工程");
        blog2.setDigest("介绍关于软件工程这门学科");
        blog2.setContent("软件工程是一门研究用工程化方法构建和维护有效、实用和高质量的软件的学科。它涉及程序设计语言、数据库、软件开发工具、系统平台、标准、设计模式等方面。在现代社会中，软件应用于多个方面。典型的软件有电子邮件、嵌入式系统、人机界面、办公套件、操作系统、编译器、数据库、游戏等。同时，各个行业几乎都有计算机软件的应用，如工业、农业、银行、航空、政府部门等。这些应用促进了经济和社会的发展，也提高了工作效率和生活效率 。");
        blog2.setAuthor("zyf");
        blog2.setCategory("软件工程");
        int ret2 = blogDao.insertBlog(blog2);
        if(ret2 == 1){
            System.out.println("添加成功");
        }*/

        //2. 查找博客(根据关键字)
        /*String key = "软";
        List<Blog> blogs = blogDao.findSelectAllOrKey(key);
        for(Blog blog : blogs){
            System.out.println(blog);
        }*/

        //3. 查找博客(根据博客id)
        /*Blog blog = blogDao.findBlogById(2);
        System.out.println(blog);*/

        //3. 删除博客(根据博客id)
        /*blogDao.deleteBlog(2);*/
    }
}
