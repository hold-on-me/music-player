package dao;

import entity.MV;
import entity.Music;
import util.DBUtils;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: ZhangYufei
 * Date: 2021-07-31
 * Time: 19:58
 */
public class MVDao {
    /**
     * 向数据库中添加 mv ，上传mv时用到
     * @param mv
     * @return
     */
    public int insert(MV mv){
        //1. 获取数据库连接
        Connection connection = DBUtils.getConnection();
        //2. 构建 SQL
        String sql = "insert into mv(title,singer,url,user_id,time) values(?,?,?,?,now())";
        PreparedStatement statement = null;
        try {
            statement = connection.prepareStatement(sql);
            statement.setString(1,mv.getTitle());
            statement.setString(2,mv.getSinger());
            statement.setString(3,mv.getUrl());
            statement.setInt(4,mv.getUserId());
            //3. 执行 SQL
            return statement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }finally {
            //4. 释放资源
            DBUtils.close(connection,statement,null);
        }
        return 0;
    }

    /**
     * 如果 key 为空，则查询全部，如果 key 不为空，则按照关键字查询 mv
     * @param key 表示模糊查询的关键字
     * @return
     */
    public List<MV> findMVAll(String key){
        Connection connection = DBUtils.getConnection();
        String sql = null;
        int active = 0;
        if(key == null || "".equals(key)) {
            sql = "select * from mv order by id desc";
        }else{
            sql = "select * from mv where title like '%" + key + "%' order by id desc";
        }
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        List<MV> list = new ArrayList<>();
        try {
            statement = connection.prepareStatement(sql);
            resultSet = statement.executeQuery();
            //System.out.println(statement.toString());
            while(resultSet.next()){
                MV mv = new MV();
                mv.setId(resultSet.getInt("id"));
                mv.setTitle(resultSet.getString("title"));
                mv.setSinger(resultSet.getString("singer"));
                mv.setUrl(resultSet.getString("url"));
                mv.setUserId(resultSet.getInt("user_id"));
                mv.setTime(resultSet.getTimestamp("time"));
                list.add(mv);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            DBUtils.close(connection,statement,resultSet);
        }
        return list;
    }

    /**
     * 显示一首 mv(根据音乐mvId)
     * @param id
     * @return
     */
    public MV findMvById(int id){
        Connection connection = DBUtils.getConnection();
        String sql = "select * from mv where id = ?";
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        try {
            statement = connection.prepareStatement(sql);
            statement.setInt(1,id);
            resultSet = statement.executeQuery();
            if(resultSet.next()){
                MV mv = new MV();
                mv.setId(resultSet.getInt("id"));
                mv.setTitle(resultSet.getString("title"));
                mv.setSinger(resultSet.getString("singer"));
                mv.setUrl(resultSet.getString("url"));
                mv.setUserId(resultSet.getInt("user_id"));
                mv.setTime(resultSet.getTimestamp("time"));
                return mv;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }finally {
            DBUtils.close(connection,statement,resultSet);
        }
        return null;
    }


    /**
     * 根据 mvId 删除 mv
     * @param mvId
     * @return
     */
    public int delete(int mvId){
        Connection connection = DBUtils.getConnection();
        String sql = "delete from mv where id=?";
        PreparedStatement statement = null;
        try {
            statement = connection.prepareStatement(sql);
            statement.setInt(1,mvId);
            return statement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }finally {
            DBUtils.close(connection,statement,null);
        }
        return 0;
    }

    /**
     * 看中间表是否有该id的mv数据
     * @param musicId
     * @return
     */
    public boolean findLoveMvById(int musicId) {
        Connection connection = DBUtils.getConnection();
        String sql = "select * from lovemv where mv_id = ?";
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        try {
            statement = connection.prepareStatement(sql);
            statement.setInt(1,musicId);
            resultSet = statement.executeQuery();
            if(resultSet.next()){
                return true;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            DBUtils.close(connection,statement,resultSet);
        }
        return false;
    }

    /**
     * 当删除服务器上的音乐时，同时在我喜欢的列表的数据库中进行删除。
     * @param mvId
     * @return
     */
    public int DeleteLoveMvById(int mvId) {
        Connection connection = DBUtils.getConnection();
        String sql = "delete from lovemv where mv_id = ?";
        PreparedStatement statement = null;
        try {
            statement = connection.prepareStatement(sql);
            statement.setInt(1,mvId);
            int ret = statement.executeUpdate();
            if (ret == 1) {
                System.out.println("删除喜欢的音乐");
            }else{
                System.out.println("没有删除成功");
            }
            return ret;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return 0;
    }


    public static void main(String[] args) {
        //MVDao mvDao = new MVDao();


        //1. 添加新的mv
        /*MV mv = new MV();
        mv.setTitle("The Nights");
        mv.setSinger("Avicii");
        mv.setUserId(1);
        mv.setUrl("mv/The Nights.mp4");
        mvDao.insert(mv);*/

        //2. 查询mv，如果 key 为空，则查询全部，如果 mv 不为空，则按照关键字查询 mv
        /*List<MV> list = mvDao.findMVAll("雅俗共赏");
        for(MV mv : list){
            System.out.println(mv);
        }*/

        //3. 删除 mv ，根据 mvId
        /*System.out.println(mvDao.delete(4));*/
    }
}
