package servlet.admin;

import dao.LoveMusicDao;
import dao.MusicDao;
import entity.Music;
import entity.User;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;

/**
 * Created with IntelliJ IDEA.
 * Description:管理员删除音乐
 * User: ZhangYufei
 * Date: 2021-07-28
 * Time: 14:47
 */
@WebServlet("/adminDelMusic")
public class AdminDelMusicServlet extends HttpServlet {
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.setCharacterEncoding("utf-8");
        resp.setContentType("application/json;charset=utf-8");

        //0. 判断该用户是否为管理员
        User user = (User) req.getSession().getAttribute("user");
        if(user == null){
            //System.out.println("管理员未登录");
            resp.sendError(404,"管理员未登录");
            return;
        }
        if(!"admin".equals(user.getUsername())){
            //System.out.println("该用户非管理员");
            resp.sendError(404,"该用户非管理员");
            return;
        }

        //1. 获取需要删除的musicId
        String musicId = req.getParameter("musicId");
        if(musicId == null){
            //System.out.println("没有需要删除的音乐");
            resp.sendError(404,"没有需要删除的音乐");
            return;
        }

        //2. 判断数据中是否存在该音乐
        MusicDao musicDao = new MusicDao();
        int mid = Integer.parseInt(musicId);
        Music music = musicDao.findMusicById(mid);
        if (music == null) {
            //System.out.println("不存在该音乐");
            resp.sendError(404,"不存在该音乐");
            return;
        }

        //3. 进行数据库方面的删除
        int ret = musicDao.delete(mid);
        if (ret == 1) {
            System.out.println("数据库删除成功");
            File file = new File(music.getUrl() + ".mp3");//指明是哪个文件
            if (file.delete()) {//从磁盘上删除文件
                System.out.println("磁盘的音乐文件删除成功");
            } else {
                System.out.println("磁盘的音乐文件删除失败");
            }
        } else {
            System.out.println("数据库删除失败");
        }

        //4. 判断在喜欢列表中是否含有该音乐的信息，如果有，则删除
        if(musicDao.findLoveMusicById(mid)){
            LoveMusicDao loveMusicDao = new LoveMusicDao();
            int res = loveMusicDao.deleteAdmin(mid);
            if(res == 1){
                System.out.println("清除数据库中喜欢的音乐成功");
            }else{
                System.out.println("清除失败");
            }
        }
    }
}
