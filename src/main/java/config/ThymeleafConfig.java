package config;

import org.thymeleaf.TemplateEngine;
import org.thymeleaf.templateresolver.ServletContextTemplateResolver;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: ZhangYufei
 * Date: 2021-07-21
 * Time: 21:23
 */
@WebListener
public class ThymeleafConfig implements ServletContextListener {
    @Override
    public void contextInitialized(ServletContextEvent servletContextEvent) {
        // 使用这个!!!
        // 这个方法就会在当前 webapp 的 ServletContext 初始化之后
        // 立即执行. Thymeleaf 的初始化就在这里调用即可!!!
        // 初始化 Thymeleaf
        //1.使用thymeleaf前，需要进行初始化
        //1) 创建 engine(引擎) 对象，负责吧java中的数据替换到模板中
        TemplateEngine engine = new TemplateEngine();
        //2) 创建 resolver(解析器) 对象，负责找到 html 模板在哪，并加载到内存中
        //只有加载到内存中才能供 engine 使用
        ServletContextTemplateResolver resolver = new ServletContextTemplateResolver(servletContextEvent.getServletContext());
        //3) 给 resolver 设置一些属性，让它能够找到 html 模板
        resolver.setCharacterEncoding("utf-8");
        resolver.setPrefix("/WEB-INF/template/");
        resolver.setSuffix(".html");
        //4) 把 resolver 和 engine 关联起来
        engine.setTemplateResolver(resolver);

        //把初始化好的 engine 对象交给 ServletContext 保管
        ServletContext context = servletContextEvent.getServletContext();
        context.setAttribute("engine",engine);

    }

    @Override
    public void contextDestroyed(ServletContextEvent servletContextEvent) {

    }
}
