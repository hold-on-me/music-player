package servlet;

import dao.UserDao;
import entity.User;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created with IntelliJ IDEA.
 * Description:注册操作
 * User: ZhangYufei
 * Date: 2021-07-28
 * Time: 14:09
 */
@WebServlet("/register")
public class RegisterServlet extends HttpServlet {
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.setCharacterEncoding("utf-8");
        resp.setContentType("application/json;charset=utf-8");

        //1. 获取需要注册的用户名、密码、年龄、性别、邮箱
        String username = req.getParameter("username");
        String password = req.getParameter("password");
        String age = req.getParameter("age");
        String gender = req.getParameter("gender");
        String email = req.getParameter("email");
        if(username == null || "".equals(username) || password == null || "".equals(password)
                || email == null || "".equals(email)){
            resp.sendError(404,"用户名/密码/邮箱不能为空");
            return;
        }

        //2. 判断该用户名在数据库中是否存在
        UserDao userDao = new UserDao();
        User user = userDao.selectByName(username);
        if (user != null) {
            resp.sendError(404,"该用户已经存在，不能重复注册");
            return;
        }

        //3. 向数据库中添加新用户
        User newUser = new User();
        newUser.setUsername(username);
        newUser.setPassword(password);
        int userAge = Integer.parseInt(age);
        newUser.setAge(userAge);
        newUser.setGender(gender);
        newUser.setEmail(email);
        userDao.insert(newUser);

        //4. 登录成功后，返回至主页
        resp.sendRedirect("index");
    }
}
