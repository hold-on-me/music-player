package servlet.admin;

import dao.UserDao;
import entity.User;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * Description:管理员查看所有用户信息
 * User: ZhangYufei
 * Date: 2021-07-28
 * Time: 14:26
 */
@WebServlet("/adminFindAllUser")
public class AdminFindAllUserServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.setCharacterEncoding("utf-8");
        resp.setContentType("application/json;charset=utf-8");

        //0. 判断该用户是否为管理员
        User user = (User) req.getSession().getAttribute("user");
        if(user == null){
            //System.out.println("管理员未登录");
            resp.sendError(404,"管理员未登录");
            return;
        }
        if(!"admin".equals(user.getUsername())){
            //System.out.println("该用户非管理员");
            resp.sendError(404,"该用户非管理员");
            return;
        }

        //1. 查看所有用户
        UserDao userDao = new UserDao();
        List<User> list = userDao.selectAllUser();
        for(User account : list){
            System.out.println(account);
        }
    }
}
