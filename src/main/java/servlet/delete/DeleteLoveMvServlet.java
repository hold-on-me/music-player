package servlet.delete;

import dao.LoveMVDao;
import dao.LoveMusicDao;
import entity.User;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: ZhangYufei
 * Date: 2021-08-01
 * Time: 16:55
 */
@WebServlet("/deleteLoveMv")
public class DeleteLoveMvServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.setCharacterEncoding("utf-8");
        resp.setContentType("application/json;charset=utf-8");

        //0. 判断用户是否进行登录
        User user = (User) req.getSession().getAttribute("user");
        if (user == null) {
            resp.sendRedirect("login");
            return;
        }

        //1. 获取需要移除的musicId
        String mvId = req.getParameter("mid");
        if (mvId == null) {
            req.getSession().setAttribute("delLoveMv","没有需要移除的mv");
            resp.sendRedirect("loveMvPage");
            return;
        }

        LoveMVDao loveMVDao = new LoveMVDao();
        //2. 判断MV是否在该用户的喜欢列表中
        int mid = Integer.parseInt(mvId);
        if(!loveMVDao.findLoveMvByMvIdAndUserId(user.getId(),mid)){
            req.getSession().setAttribute("delLoveMv","该mv不在你喜欢的列表中");
            resp.sendRedirect("loveMvPage");
            return;
        }

        //3. 将该用户需要移除的mv从喜欢列表中移除
        int ret = loveMVDao.delete(user.getId(),mid);
        if (ret == 1) {
            System.out.println("移除成功");
        }else{
            System.out.println("移除失败");
        }

        resp.sendRedirect("loveMvPage");
    }
}
