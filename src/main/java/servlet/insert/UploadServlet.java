package servlet.insert;

import dao.MusicDao;
import entity.Music;
import entity.User;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;
import java.io.IOException;
import java.net.URLDecoder;

/**
 * Created with IntelliJ IDEA.
 * Description:上传音乐(前提是登录)
 * User: ZhangYufei
 * Date: 2021-07-27
 * Time: 13:34
 */
@WebServlet("/upload")
@MultipartConfig
public class UploadServlet extends HttpServlet {
    //文件的存储位置
    private final String PATH = "/root/apache-tomcat-8.5.69/webapps/music/";

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.setCharacterEncoding("utf-8");
        resp.setContentType("application/json;charset=utf-8");
        //判断是否登录
        User user = (User) req.getSession().getAttribute("user");
        if(user == null){
            /*System.out.println("还未进行登录不能上传");
            req.setAttribute("Msg","还未进行登录不能上传");*/
            resp.sendRedirect("login");
            return;
        }
        //先上传服务器，如果使用postman测试，则需要进行解码操作
        Part part = req.getPart("filename");

        String header = part.getHeader("Content-Disposition");
        //System.out.println(header);
        int start = header.lastIndexOf("=");
        String filename = header.substring(start + 2,header.length()-1).replace("/","");
        //System.out.println("filename:::" + filename);
        /*//谷歌浏览器会自动解析 postman测试需要处理
        int index = filename.indexOf("%");
        filename = filename.substring(index);
        //对音乐名解码
        filename = URLDecoder.decode(filename,"utf-8");*/
        //System.out.println(filename);
        part.write(PATH + "/music/" + filename); //将文件写入(上传)到服务器指定路径下

        //获取歌手名字
        String singer = req.getParameter("singer");

        Music music = new Music();
        String[] name = filename.split("\\.");
        music.setTitle(name[0]);
        music.setSinger(singer);
        music.setUrl("music/" + filename);
        music.setUserId(user.getId());
        MusicDao musicDao = new MusicDao();
        musicDao.insert(music);

        //上传成功后，返回值音乐列表页面
        resp.sendRedirect("musicPage");
    }
}
