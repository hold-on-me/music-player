package servlet.delete;

import dao.LoveMusicDao;
import entity.User;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created with IntelliJ IDEA.
 * Description:从喜欢的列表中移除该音乐信息
 * User: ZhangYufei
 * Date: 2021-07-28
 * Time: 13:51
 */
@WebServlet("/deleteLoveMusic")
public class DeleteLoveMusicServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.setCharacterEncoding("utf-8");
        resp.setContentType("application/json;charset=utf-8");

        //0. 判断用户是否进行登录
        User user = (User) req.getSession().getAttribute("user");
        if (user == null) {
            //System.out.println("还未进行登录，不能移除喜欢列表中的音乐");
            resp.sendRedirect("login");
            return;
        }

        //1. 获取需要移除的musicId
        String musicId = req.getParameter("mid");
        if (musicId == null) {
            //System.out.println("没有需要移除的音乐");
            req.getSession().setAttribute("delLoveMusic","没有需要移除的音乐");
            resp.sendRedirect("loveMusicPage");
            return;
        }

        LoveMusicDao loveMusicDao = new LoveMusicDao();
        //2. 判断音乐是否在该用户的喜欢列表中
        int mid = Integer.parseInt(musicId);
        if(!loveMusicDao.findLoveMusicByMusicIdAndUserId(user.getId(),mid)){
            //System.out.println("该音乐不在用户喜欢的列表中");
            req.getSession().setAttribute("delLoveMusic","该音乐不在你喜欢的列表中");
            resp.sendRedirect("loveMusicPage");
            return;
        }

        //3. 将该用户需要移除的音乐从喜欢列表中移除
        int ret = loveMusicDao.delete(user.getId(),mid);
        if (ret == 1) {
            System.out.println("移除成功");
        }else{
            System.out.println("移除失败");
        }

        resp.sendRedirect("loveMusicPage");
    }
}
