package servlet;

import com.fasterxml.jackson.databind.ObjectMapper;
import dao.UserDao;
import entity.User;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.WebContext;
import service.BackService;
import util.DBUtils;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.sql.Connection;
import java.util.HashMap;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * Description:登录操作
 * User: ZhangYufei
 * Date: 2021-07-18
 * Time: 10:21
 */
@WebServlet("/login")
public class LoginServlet extends HttpServlet {
    private String isCorr(HttpServletRequest req, HttpServletResponse resp,String text) throws ServletException, IOException {
        WebContext webContext = new WebContext(req, resp, getServletContext());
        webContext.setVariable("isCorrect",text);
        TemplateEngine engine = (TemplateEngine) getServletContext().getAttribute("engine");
        return engine.process("login", webContext);
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.setCharacterEncoding("utf-8");
        //resp.setContentType("application/json;charset=utf-8");
        resp.setContentType("text/html;charset=utf-8");
        String html = isCorr(req,resp,"");
        resp.getWriter().write(html);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.setCharacterEncoding("utf-8");
        //resp.setContentType("application/json;charset=utf-8");
        resp.setContentType("text/html;charset=utf-8");

        String username = req.getParameter("username");
        String password = req.getParameter("password");

        //Map<String,Object> return_map = new HashMap<>();
        String isCorrect = "";
        if("".equals(username) || "".equals(password) || username == null || password == null){
            isCorrect = "用户名或密码不能为空";
            String html = isCorr(req,resp,isCorrect);
            resp.getWriter().write(html);
            return;
        }
        UserDao userDao = new UserDao();
        User user = userDao.login(username,password);
        if (user == null) {
            isCorrect = "用户名或密码错误";
            String html = isCorr(req,resp,isCorrect);
            resp.getWriter().write(html);
            return;
        }else if(!password.equals(user.getPassword())) {
            // 密码不匹配
            isCorrect = "用户名或密码错误";
            String html = isCorr(req,resp,isCorrect);
            resp.getWriter().write(html);
            return;
        }else{
            req.getSession().setAttribute("user",user);
        }

       // ObjectMapper mapper = new ObjectMapper();
        //利用Jackson将map转化为json对象
        //writer 将转换后的json字符串保存到字符输出流中，最后返回给客户端
        //mapper.writeValue(resp.getWriter(),return_map);
        //resp.sendRedirect("findMusic");
        HttpSession session = req.getSession(true);
        session.setAttribute("user",user);
        BackService backService = new BackService();
        backService.back(resp,"index");
    }
}
