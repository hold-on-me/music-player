package dao;

import entity.User;
import util.DBUtils;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * Description:对用户的操作，增删改查
 * User: ZhangYufei
 * Date: 2021-07-18
 * Time: 9:28
 */
public class UserDao {
    //验证登录
    public User login(String username,String password){
        Connection connection = DBUtils.getConnection();
        String sql = "select * from user where username = ? and password = ?";
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        User user = null;
        try {
            statement = connection.prepareStatement(sql);
            statement.setString(1,username);
            statement.setString(2,password);
            resultSet = statement.executeQuery();
            if(resultSet.next()){
                user = new User();
                user.setId(resultSet.getInt("id"));
                user.setUsername(resultSet.getString("username"));
                user.setPassword(resultSet.getString("password"));
                user.setAge(resultSet.getInt("age"));
                user.setGender(resultSet.getString("gender"));
                user.setEmail(resultSet.getString("email"));
            }else {
                System.out.println("用户名或者密码错误，登录失败");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            DBUtils.close(connection,statement,null);
        }
        return user;
    }

    //向数据库中添加用户信息
    public void insert(User user) {
        Connection connection = DBUtils.getConnection();
        String sql = sql = "insert into user(username,password,age,gender,email) values(?,?,?,?,?)";
        PreparedStatement statement = null;
        try {
            statement = connection.prepareStatement(sql);
            statement.setString(1, user.getUsername());
            statement.setString(2, user.getPassword());
            statement.setInt(3, user.getAge());
            statement.setString(4, user.getGender());
            statement.setString(5, user.getEmail());
            int ret = statement.executeUpdate();
            if (ret == 1) {
                System.out.println("注册成功");
            }else{
                System.out.println("注册失败");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            DBUtils.close(connection, statement, null);
        }
    }

    //根据用户名查询数据库（查询单个用户的信息）
    public User selectByName(String username) {
        Connection connection = DBUtils.getConnection();
        String sql = "select * from user where username = ?";
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        try {
            statement = connection.prepareStatement(sql);
            statement.setString(1, username);
            resultSet = statement.executeQuery();
            if (resultSet.next()) {
                User user = new User();
                user.setId(resultSet.getInt("id"));
                user.setUsername(resultSet.getString("username"));
                user.setPassword(resultSet.getString("password"));
                user.setAge(resultSet.getInt("age"));
                user.setGender(resultSet.getString("gender"));
                user.setEmail(resultSet.getString("email"));
                return user;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            DBUtils.close(connection, statement, resultSet);
        }
        return null;
    }


    //查询所有用户的信息
    public List<User> selectAllUser(){
        Connection connection = DBUtils.getConnection();
        String sql = "select * from user";
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        List<User> list = new ArrayList<>();
        try {
            statement = connection.prepareStatement(sql);
            resultSet = statement.executeQuery();
            while (resultSet.next()){
                User user = new User();
                user.setId(resultSet.getInt("id"));
                user.setUsername(resultSet.getString("username"));
                user.setPassword(resultSet.getString("password"));
                user.setAge(resultSet.getInt("age"));
                user.setGender(resultSet.getString("gender"));
                user.setEmail(resultSet.getString("email"));
                list.add(user);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            DBUtils.close(connection,statement,resultSet);
        }
        return list;
    }

    //修改密码
    public boolean updatePassword(int userId,String password){
        Connection connection = DBUtils.getConnection();
        String sql = "update user set password=? where id=?";
        PreparedStatement statement = null;
        try {
            statement = connection.prepareStatement(sql);
            statement.setString(1,password);
            statement.setInt(2,userId);
            int ret = statement.executeUpdate();
            if (ret == 1) {
                return true;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }finally {
            DBUtils.close(connection,statement,null);
        }
        return false;
    }

    //删除用户
    public int deleteUser(String username) {
        Connection connection = DBUtils.getConnection();
        String sql = "delete from user where username = ?";
        PreparedStatement statement = null;
        try {
            statement = connection.prepareStatement(sql);
            statement.setString(1, username);
            return statement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            DBUtils.close(connection, statement, null);
        }
        return 0;
    }

    public static void main(String[] args) {
        UserDao userDao = new UserDao();
        //1. 测试添加用户
        /*User user = new User();
        user.setUsername("test");
        user.setPassword("123");
        user.setAge(20);
        user.setGender("男");
        user.setEmail("test@163.com");
        userDao.insert(user);*/

        //2. 测试显示所有用户
        /*List<User> list = userDao.selectAllUser();
        for(User user:list){
            System.out.println(user);
        }*/

        //3. 测试显示一个用户，根据 username
        /*User user = userDao.selectByName("admin");
        System.out.println(user);*/

        //4. 测试删除用户
        /*userDao.deleteUser("test01");*/

        //5. 测试修改密码
        /*System.out.println(userDao.updatePassword(1,"123"));*/
    }
}
