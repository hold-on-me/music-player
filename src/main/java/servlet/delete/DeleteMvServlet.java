package servlet.delete;

import dao.LoveMVDao;
import dao.LoveMusicDao;
import dao.MVDao;
import dao.MusicDao;
import entity.MV;
import entity.Music;
import entity.User;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: ZhangYufei
 * Date: 2021-08-01
 * Time: 15:09
 */
@WebServlet("/deleteMv")
public class DeleteMvServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.setCharacterEncoding("utf-8");
        resp.setContentType("application/json;charset=utf-8");

        //0. 判断是否登录，只能删除自己发布的 mv
        User user = (User) req.getSession().getAttribute("user");
        if (user == null) {
            resp.sendRedirect("login");
            return;
        }

        String mvId = req.getParameter("mid");
        //1. 判断请求删除的 mvId 是否为空
        if (mvId == null) {
            req.getSession().setAttribute("delMv","没有需要删除的mvId");
            resp.sendRedirect("mvPage");
            return;
        }

        MVDao mvDao = new MVDao();
        int mid = Integer.parseInt(mvId);
        MV mv = mvDao.findMvById(mid);
        //2. 判断数据库中是否存在该 mvId
        if (mv == null) {
            req.getSession().setAttribute("delMv","没有需要删除的mvId");
            resp.sendRedirect("mvPage");
            return;
        }
        //3. 判断要删除的 mv 是不是自己发布的
        if (mv.getUserId() != user.getId()) {
            req.getSession().setAttribute("delMv","你不能删除别人发布的 mv");
            resp.sendRedirect("mvPage");
            return;
        }
        //4. 此时说明：需要删除的mvId存在，并且发布者和删除者是同一个人，可以进行删除操作
        int ret = mvDao.delete(mid);//首先删除数据库
        if (ret == 1) {
            System.out.println("数据库删除成功");
            File file = new File("/root/apache-tomcat-8.5.69/webapps/music/" + mv.getUrl());//指明是哪个文件
            if (file.delete()) {//从磁盘上删除文件
                System.out.println("磁盘的 mv 文件删除成功");
            } else {
                System.out.println("磁盘的 mv 文件删除失败");
            }
        } else {
            System.out.println("数据库删除失败");
        }

        //5. 判断在喜欢列表中是否含有该mv的信息，如果有，则删除
        if(mvDao.findLoveMvById(mid)){
            LoveMVDao loveMvDao = new LoveMVDao();
            int res = loveMvDao.delete(user.getId(),mid);
            if(res == 1){
                System.out.println("清除数据库中喜欢的 mv 成功");
            }else{
                System.out.println("清除失败");
            }
        }

        resp.sendRedirect("mvPage");
    }
}
