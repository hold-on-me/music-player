package dao;

import entity.Music;
import util.DBUtils;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * Description:对用户喜欢的音乐进行查询
 * User: ZhangYufei
 * Date: 2021-07-26
 * Time: 10:07
 */
public class LoveMusicDao {
    //添加喜欢的音乐
    public boolean insert(int userId,int musicId){
        Connection connection = DBUtils.getConnection();
        String sql = "insert into lovemusic(user_id,music_id) values(?,?)";
        PreparedStatement statement = null;
        try {
            statement = connection.prepareStatement(sql);
            statement.setInt(1,userId);
            statement.setInt(2,musicId);
            int ret = statement.executeUpdate();
            if (ret == 1) {
                return true;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            DBUtils.close(connection,statement,null);
        }
        return false;
    }

    //根据用户id，显示该用户喜欢的所有音乐
    public List<Music> findLoveMusic(int userId){
        Connection connection = DBUtils.getConnection();
        String sql = "select music.id,music.title,music.singer,music.url,music.time " +
                "from lovemusic,music where music.id = lovemusic.music_id and lovemusic.user_id = ?";
        List<Music> list = new ArrayList<>();
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        try {
            statement = connection.prepareStatement(sql);
            statement.setInt(1,userId);
            resultSet = statement.executeQuery();
            while (resultSet.next()) {
                Music music = new Music();
                music.setId(resultSet.getInt("music.id"));
                music.setTitle(resultSet.getString("title"));
                music.setSinger(resultSet.getString("singer"));
                music.setUrl(resultSet.getString("url"));
                music.setTime(resultSet.getTimestamp("time"));
                list.add(music);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            DBUtils.close(connection,statement,resultSet);
        }
        return list;
    }

    //添加喜欢的音乐的时候，需要先判断该音乐是否存在，不重复添加喜欢的音乐
    public boolean findLoveMusicByMusicIdAndUserId(int userId,int musicId){
        Connection connection = DBUtils.getConnection();
        String sql = "select * from lovemusic where user_id = ? and music_id = ?";
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        try {
            statement = connection.prepareStatement(sql);
            statement.setInt(1,userId);
            statement.setInt(2,musicId);
            resultSet = statement.executeQuery();
            if(resultSet.next()){
                return true;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            DBUtils.close(connection,statement,null);
        }
        return false;
    }

    //模糊查询喜欢的音乐
    public List<Music> findMusicByKeyAndUID(String str,int userId){
        Connection connection = DBUtils.getConnection();
        String sql = "select music.id,music.title,music.singer,music.url,music.time from lovemusic,music" +
                " where music.id = lovemusic.music_id and lovemusic.user_id = ? and music.title like '%" + str + "%'";
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        List<Music> list = new ArrayList<>();
        try {
            statement = connection.prepareStatement(sql);
            statement.setInt(1,userId);
            resultSet = statement.executeQuery();
            //System.out.println(statement.toString()); //打印执行的 SQL 语句
            while (resultSet.next()) {
                Music music = new Music();
                music.setId(resultSet.getInt("music.id"));
                music.setTitle(resultSet.getString("title"));
                music.setSinger(resultSet.getString("singer"));
                music.setUrl(resultSet.getString("url"));
                music.setTime(resultSet.getTimestamp("time"));
                list.add(music);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            DBUtils.close(connection,statement,null);
        }
        return list;
    }

    //删除喜欢的音乐
    public int delete(int userId,int musicId){
        Connection connection = DBUtils.getConnection();
        String sql = "delete from lovemusic where user_id = ? and music_id = ?";
        PreparedStatement statement = null;
        try {
            statement = connection.prepareStatement(sql);
            statement.setInt(1,userId);
            statement.setInt(2,musicId);
            return statement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            DBUtils.close(connection,statement,null);
        }
        return 0;
    }

    //删除喜欢的音乐
    public int deleteAdmin(int musicId){
        Connection connection = DBUtils.getConnection();
        String sql = "delete from lovemusic where music_id = ?";
        PreparedStatement statement = null;
        try {
            statement = connection.prepareStatement(sql);
            statement.setInt(1,musicId);
            return statement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            DBUtils.close(connection,statement,null);
        }
        return 0;
    }

    public static void main(String[] args) {
        LoveMusicDao loveMusicDao = new LoveMusicDao();
        //1. 添加新的喜欢的音乐，以及测试重复添加
        /*if(!loveMusicDao.findLoveMusicByMusicIdAndUserId(2,4)) {
            loveMusicDao.insert(2, 4);
        }*/

        //2. 查询用户id喜欢的音乐
        /*List<Music> list = loveMusicDao.findLoveMusic(4);
        for(Music music:list){
            System.out.println(music);
        }*/

        //3. 模糊查询喜欢的音乐
        /*List<Music> list = loveMusicDao.findMusicByKeyAndUID("see",4);
        for(Music music:list){
            System.out.println(music);
        }*/

        //4. 删除喜欢的音乐
        /*int ret = loveMusicDao.delete(2,3);
        System.out.println(ret);*/
    }

}
