package dao;

import entity.Music;
import util.DBUtils;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * Description:对音乐的操作
 * User: ZhangYufei
 * Date: 2021-07-26
 * Time: 9:10
 */
public class MusicDao {
    //添加音乐
    public int insert(Music music){
        //1. 获取数据库的连接
        Connection connection = DBUtils.getConnection();
        //2. 构建 SQL 语句
        String sql = "insert into music(title,singer,url,user_id,time) values(?,?,?,?,now())";
        PreparedStatement statement = null;
        try {
            statement = connection.prepareStatement(sql);
            statement.setString(1,music.getTitle());
            statement.setString(2,music.getSinger());
            statement.setString(3,music.getUrl());
            statement.setInt(4,music.getUserId());
            //3. 执行 SQL 语句
            int ret = statement.executeUpdate();
            if(ret == 0){
                return 0; //添加音乐失败
            }
            return 1; //添加成功
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            DBUtils.close(connection,statement,null);
        }
        return 0;
    }

    //显示所有音乐
    public List<Music> findMusicAll(){
        Connection connection = DBUtils.getConnection();
        String sql = "select * from music order by id desc";
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        List<Music> list = new ArrayList<>();
        try {
            statement = connection.prepareStatement(sql);
            resultSet = statement.executeQuery();
            while (resultSet.next()){
                Music music = new Music();
                music.setId(resultSet.getInt("id"));
                music.setTitle(resultSet.getString("title"));
                music.setSinger(resultSet.getString("singer"));
                music.setUrl(resultSet.getString("url"));
                music.setUserId(resultSet.getInt("user_id"));
                music.setTime(resultSet.getTimestamp("time"));
                list.add(music);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            DBUtils.close(connection,statement,resultSet);
        }
        return list;
    }

    //显示一首音乐(根据音乐id)
    public Music findMusicById(int id){
        Connection connection = DBUtils.getConnection();
        String sql = "select * from music where id = ?";
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        try {
            statement = connection.prepareStatement(sql);
            statement.setInt(1,id);
            resultSet = statement.executeQuery();
            if(resultSet.next()){
                Music music = new Music();
                music.setId(resultSet.getInt("id"));
                music.setTitle(resultSet.getString("title"));
                music.setSinger(resultSet.getString("singer"));
                music.setUrl(resultSet.getString("url"));
                music.setUserId(resultSet.getInt("user_id"));
                music.setTime(resultSet.getTimestamp("time"));
                return music;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }finally {
            DBUtils.close(connection,statement,resultSet);
        }
        return null;
    }

    //模糊查询，显示音乐
    public List<Music> findMusicByKey(String str){
        Connection connection = DBUtils.getConnection();
        String sql = "select * from music where title like '%" + str + "%' order by id desc";
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        List<Music> list = new ArrayList<>();
        try {
            statement = connection.prepareStatement(sql);
            //statement.setString(1,str);
            resultSet = statement.executeQuery();
            while (resultSet.next()) {
                Music music = new Music();
                music.setId(resultSet.getInt("id"));
                music.setTitle(resultSet.getString("title"));
                music.setSinger(resultSet.getString("singer"));
                music.setUrl(resultSet.getString("url"));
                music.setUserId(resultSet.getInt("user_id"));
                music.setTime(resultSet.getTimestamp("time"));
                list.add(music);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }finally {
            DBUtils.close(connection,statement,resultSet);
        }
        return list;
    }

    //删除音乐(根据音乐id)
    public int delete(int id){
        Connection connection = DBUtils.getConnection();
        String sql = "delete from music where id = ?";
        PreparedStatement statement = null;
        try {
            statement = connection.prepareStatement(sql);
            statement.setInt(1,id);
            return statement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            DBUtils.close(connection,statement,null);
        }
        return 0;
    }

    //看中间表是否有该id的音乐数据
    public boolean findLoveMusicById(int musicId) {
        Connection connection = DBUtils.getConnection();
        String sql = "select * from lovemusic where music_id = ?";
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        try {
            statement = connection.prepareStatement(sql);
            statement.setInt(1,musicId);
            resultSet = statement.executeQuery();
            if(resultSet.next()){
                return true;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            DBUtils.close(connection,statement,resultSet);
        }
        return false;
    }

    //当删除服务器上的音乐时，同时在我喜欢的列表的数据库中进行删除。
    public int DeleteLoveMusicById(int musicId) {
        Connection connection = DBUtils.getConnection();
        String sql = "delete from lovemusic where music_id = ?";
        PreparedStatement statement = null;
        try {
            statement = connection.prepareStatement(sql);
            statement.setInt(1,musicId);
            int ret = statement.executeUpdate();
            if (ret == 1) {
                System.out.println("删除喜欢的音乐");
            }else{
                System.out.println("没有删除成功");
            }
            return ret;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return 0;
    }

    public static void main(String[] args) {
        MusicDao musicDao = new MusicDao();
        //1. 测试添加音乐
        /*Music music = new Music();
        music.setTitle("Superstar");
        music.setSinger("MARINA");
        music.setUrl("/music");
        music.setUserId(2);
        musicDao.insert(music);*/
        //2. 测试显示所有音乐
        /*List<Music> list = musicDao.findMusicAll();
        for(Music music:list){
            System.out.println(music);
        }*/
        //3. 测试显示一首音乐，根据音乐id
        /*Music music = musicDao.findMusicById(4);
        System.out.println(music);*/
        //4. 测试删除，根据音乐id
        /*musicDao.delete(5);*/

        //5. 测试模糊查询
        /*List<Music> list = musicDao.findMusicByKey("to");
        for(Music music:list){
            System.out.println(music);
        }*/

        //6. 中间表查询是否有该id的音乐
        /*boolean isHave = musicDao.findLoveMusicById(2);
        System.out.println(isHave);*/
    }
}
