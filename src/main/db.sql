create database if not exists MusicServer;
use MusicServer;

drop table if exists music;
create table music(
    id int primary key auto_increment,
    title varchar(50) not null,
    singer varchar(50) not null,
    url varchar(100) not null,
    user_id int not null,
    time datetime
)charset=utf8;

drop table if exists mv;
create table mv(
    id int primary key auto_increment,
    title varchar(50) not null,
    singer varchar(50) not null,
    url varchar(100) not null,
    user_id int not null,
    time datetime
)charset=utf8;

drop tables if exists user;
create table user(
    id int primary key auto_increment,
    username varchar(15) not null unique,
    password varchar(15) not null,
    age int not null,
    gender varchar(2) not null,
    email varchar(30) not null
)charset=utf8;

drop tables if exists lovemusic;
create table lovemusic(
    id int primary key auto_increment,
    user_id int(11) not null,
    music_id int(11) not null
)charset=utf8;

drop tables if exists lovemv;
create table lovemv(
    id int primary key auto_increment,
    user_id int(11) not null,
    mv_id int(11) not null
)charset=utf8;

drop table if exists blog;
create table blog(
    id int primary key auto_increment,
    title varchar(512),
    digest varchar(512),
    content text,
    author varchar(15),
    postTime datetime,
    category varchar(20)
)charset=utf8;

drop table if exists comment;
create table comment(
    id int primary key auto_increment,
    commentUser varchar(15),
    commentTime datetime,
    content text,
    blog_id int not null,
    foreign key(blog_id) references blog(id)
)charset=utf8;
