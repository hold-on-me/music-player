package dao;

import entity.Comment;
import util.DBUtils;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: ZhangYufei
 * Date: 2021-08-02
 * Time: 20:59
 */
public class CommentDao {
    /**
     * 添加评论
     * @param comment
     * @return
     */
    public int insertComment(Comment comment){
        //1. 获取数据库连接
        Connection connection = DBUtils.getConnection();
        //2. 构建 SQL
        String sql = "insert into comment(commentUser,commentTime,content,blog_id) values(?,now(),?,?)";
        PreparedStatement statement = null;
        try {
            statement = connection.prepareStatement(sql);
            statement.setString(1,comment.getCommentUser());
            statement.setString(2,comment.getContent());
            statement.setInt(3,comment.getBlogId());
            //3. 执行 SQL
            return statement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            //4. 释放资源
            DBUtils.close(connection,statement,null);
        }
        return 0;
    }


    /**
     * 删除评论
     * @param commentId
     * @return
     */
    public int deleteComment(int commentId){
        Connection connection = DBUtils.getConnection();
        String sql = "delete from comment where id = ?";
        PreparedStatement statement = null;
        try {
            statement = connection.prepareStatement(sql);
            statement.setInt(1,commentId);
            return statement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            DBUtils.close(connection,statement,null);
        }
        return 0;
    }

    /**
     * 显示该篇博客的所有评论
     * @return
     */
    public List<Comment> findCommentAll(int blogId){
        Connection connection = DBUtils.getConnection();
        String sql = "select * from comment where blog_id = ?  order by commentTime desc";
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        List<Comment> list = new ArrayList<>();
        try {
            statement = connection.prepareStatement(sql);
            statement.setInt(1,blogId);
            resultSet = statement.executeQuery();
            while(resultSet.next()){
                Comment comment = new Comment();
                comment.setId(resultSet.getInt("id"));
                comment.setCommentUser(resultSet.getString("commentUser"));
                comment.setContent(resultSet.getString("content"));
                comment.setCommentTime(resultSet.getTimestamp("commentTime"));
                comment.setBlogId(resultSet.getInt("blog_id"));
                list.add(comment);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            DBUtils.close(connection,statement,resultSet);
        }
        return list;
    }

    /**
     * 根据 commentId 查找评论是否存在
     * @param commentId
     * @return
     */
    public boolean findCommentById(int commentId,String commentUser){
        Connection connection = DBUtils.getConnection();
        String sql = null;
        boolean active = false;
        if(commentUser == null || "".equals(commentUser)){
            sql = "select * from comment where id = ?";
        }else{
            sql = "select * from comment where id=? and commentUser=?";
            active = true;
        }

        PreparedStatement statement = null;
        ResultSet resultSet = null;
        try {
            statement = connection.prepareStatement(sql);
            statement.setInt(1,commentId);
            if(active){
                statement.setString(2,commentUser);
            }
            resultSet = statement.executeQuery();
            if(resultSet.next()){
                return true;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            DBUtils.close(connection,statement,resultSet);
        }
        return false;
    }

    /**
     * 根据博客id查找评论，如果需要删除该篇博客，则需要删除该篇博客的所以评论
     * @param blogId
     * @return
     */
    public List<Comment> findCommentByBlogId(int blogId){
        Connection connection = DBUtils.getConnection();
        String sql = "select * from comment where blog_id=?";
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        List<Comment> list = new ArrayList<>();
        try {
            statement = connection.prepareStatement(sql);
            statement.setInt(1,blogId);
            resultSet = statement.executeQuery();
            while (resultSet.next()){
                Comment comment = new Comment();
                comment.setId(resultSet.getInt("id"));
                comment.setCommentUser(resultSet.getString("commentUser"));
                comment.setContent(resultSet.getString("content"));
                comment.setCommentTime(resultSet.getTimestamp("commentTime"));
                comment.setBlogId(resultSet.getInt("blog_id"));
                list.add(comment);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            DBUtils.close(connection,statement,resultSet);
        }
        return list;
    }

    public static void main(String[] args) {
        CommentDao commentDao = new CommentDao();
        //1. 添加评论
        /*Comment comment1 = new Comment();
        comment1.setCommentUser("admin");
        comment1.setContent("该篇操作系统的知识讲的很好");
        comment1.setBlogId(1);
        int ret1 = commentDao.insertComment(comment1);
        if(ret1 == 1){
            System.out.println("添加成功");
        }
        Comment comment2 = new Comment();
        comment2.setCommentUser("zhangsan");
        comment2.setContent("该篇软件工程的知识讲的很好");
        comment2.setBlogId(3);
        int ret2 = commentDao.insertComment(comment2);
        if(ret2 == 1){
            System.out.println("添加成功");
        }*/

        //2. 显示所有评论
        /*List<Comment> list = commentDao.findCommentAll(3);
        for(Comment comment : list){
            System.out.println(comment);
        }*/

        //3.删除评论
        /*System.out.println(commentDao.deleteComment(2));*/

        //4. 根据博客id查看所有评论
        /*List<Comment> list = commentDao.findCommentByBlogId(3);
        for(Comment comment : list){
            System.out.println(comment);
        }*/
    }
}
