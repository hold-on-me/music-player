package dao;

import entity.MV;
import util.DBUtils;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: ZhangYufei
 * Date: 2021-08-01
 * Time: 13:57
 */
public class LoveMVDao {
    //添加喜欢的 MV
    public boolean insert(int userId,int mvId){
        Connection connection = DBUtils.getConnection();
        String sql = "insert into lovemv(user_id,mv_id) values(?,?)";
        PreparedStatement statement = null;
        try {
            statement = connection.prepareStatement(sql);
            statement.setInt(1,userId);
            statement.setInt(2,mvId);
            int ret = statement.executeUpdate();
            if (ret == 1) {
                return true;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            DBUtils.close(connection,statement,null);
        }
        return false;
    }

    //根据用户id查询mv，如果key为空，则查询全部，如果key不为空，则模糊查询
    public List<MV> findLoveMv(String key,int userId){
        Connection connection = DBUtils.getConnection();
        String sql = null;
        if(key == null || "".equals(key)) {
            sql = "select mv.id,mv.title,mv.singer,mv.url,mv.time " +
                    "from lovemv,mv where mv.id = lovemv.mv_id and lovemv.user_id = ?";
        }else{
            sql = "select mv.id,mv.title,mv.singer,mv.url,mv.time from lovemv,mv" +
                    " where mv.id = lovemv.mv_id and lovemv.user_id = ? and mv.title like '%" + key + "%'";
        }
        List<MV> list = new ArrayList<>();
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        try {
            statement = connection.prepareStatement(sql);
            statement.setInt(1,userId);
            resultSet = statement.executeQuery();
            while (resultSet.next()) {
                MV mv = new MV();
                mv.setId(resultSet.getInt("mv.id"));
                mv.setTitle(resultSet.getString("title"));
                mv.setSinger(resultSet.getString("singer"));
                mv.setUrl(resultSet.getString("url"));
                mv.setTime(resultSet.getTimestamp("time"));
                list.add(mv);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            DBUtils.close(connection,statement,resultSet);
        }
        return list;
    }

    //添加喜欢的音乐的时候，需要先判断该音乐是否存在，不重复添加喜欢的音乐
    public boolean findLoveMvByMvIdAndUserId(int userId,int mvId){
        Connection connection = DBUtils.getConnection();
        String sql = "select * from lovemv where user_id = ? and mv_id = ?";
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        try {
            statement = connection.prepareStatement(sql);
            statement.setInt(1,userId);
            statement.setInt(2,mvId);
            resultSet = statement.executeQuery();
            if(resultSet.next()){
                return true;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            DBUtils.close(connection,statement,null);
        }
        return false;
    }

    //删除喜欢的音乐
    public int delete(int userId,int mvId){
        Connection connection = DBUtils.getConnection();
        String sql = "delete from lovemv where user_id = ? and mv_id = ?";
        PreparedStatement statement = null;
        try {
            statement = connection.prepareStatement(sql);
            statement.setInt(1,userId);
            statement.setInt(2,mvId);
            return statement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            DBUtils.close(connection,statement,null);
        }
        return 0;
    }

    //删除喜欢的音乐
    public int deleteAdmin(int mvId){
        Connection connection = DBUtils.getConnection();
        String sql = "delete from lovemv where mv_id = ?";
        PreparedStatement statement = null;
        try {
            statement = connection.prepareStatement(sql);
            statement.setInt(1,mvId);
            return statement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            DBUtils.close(connection,statement,null);
        }
        return 0;
    }
    public static void main(String[] args) {
        LoveMVDao loveMvDao = new LoveMVDao();
        //1. 添加新的喜欢的音乐，以及测试重复添加
        /*if(!loveMvDao.findLoveMvByMvIdAndUserId(1,2)) {
            loveMvDao.insert(1, 2);
        }*/

        //2. 查询用户id喜欢的音乐，两个参数，key和userId
        /*List<MV> list = loveMvDao.findLoveMv("许",1);
        for(MV mv:list){
            System.out.println(mv);
        }*/

        //4. 删除喜欢的音乐
       /* int ret = loveMvDao.delete(1,1);
        System.out.println(ret);*/
    }

}
