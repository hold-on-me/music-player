package entity;

import java.sql.Timestamp;

/**
 * Created with IntelliJ IDEA.
 * Description:音乐实体
 * User: ZhangYufei
 * Date: 2021-07-18
 * Time: 8:31
 */
public class Music {
    private int id;
    private String title;
    private String singer;
    private String url;
    private Timestamp time;
    private int userId;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getSinger() {
        return singer;
    }

    public void setSinger(String singer) {
        this.singer = singer;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Timestamp getTime() {
        return time;
    }

    public void setTime(Timestamp time) {
        this.time = time;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    @Override
    public String toString() {
        return "Music{" +
                "id=" + id +
                ", title='" + title + '\'' +
                ", singer='" + singer + '\'' +
                ", url='" + url + '\'' +
                ", time=" + time +
                ", userId=" + userId +
                '}';
    }
}
