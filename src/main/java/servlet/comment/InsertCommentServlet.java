package servlet.comment;

import dao.CommentDao;
import entity.Comment;
import entity.User;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.WebContext;
import service.LoginService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: ZhangYufei
 * Date: 2021-08-03
 * Time: 13:06
 */
@WebServlet("/insertComment")
public class InsertCommentServlet extends HttpServlet {
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.setCharacterEncoding("utf-8");
        resp.setContentType("application/json;charset=utf-8");

        //0. 判断用户是否登录，如果没有登录不能进行评论
        LoginService loginService = new LoginService();
        User user = loginService.checkLogin(req);
        if(user == null){
            resp.sendRedirect("login");
            return;
        }

        //1. 获取博客id和评论的信息
        String message = req.getParameter("message");
        String blogId = req.getParameter("id");
        if(message == null || "".equals(message)){
            req.getSession().setAttribute("comment","如果想要评论的话就必须填写评论信息");
            resp.sendRedirect("blogDetail?id="+blogId);
            return;
        }

        //2. 创建 Comment 对象
        Comment comment = new Comment();
        comment.setCommentUser(user.getUsername());
        comment.setContent(message);
        comment.setBlogId(Integer.parseInt(blogId));

        //3. 向数据库中添加评论
        CommentDao commentDao = new CommentDao();
        commentDao.insertComment(comment);

        //4. 添加成功后返回该篇博客
        resp.sendRedirect("blogDetail?id=" + blogId);
    }
}
