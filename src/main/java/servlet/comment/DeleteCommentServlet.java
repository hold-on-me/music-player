package servlet.comment;

import dao.CommentDao;
import entity.User;
import service.LoginService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: ZhangYufei
 * Date: 2021-08-03
 * Time: 13:06
 */
@WebServlet("/deleteComment")
public class DeleteCommentServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.setCharacterEncoding("utf-8");
        resp.setContentType("application/json;charset=utf-8");

        //0. 判断用户是否登录，如果没有登录不能进行评论
        LoginService loginService = new LoginService();
        User user = loginService.checkLogin(req);
        if(user == null){
            resp.sendRedirect("login");
            return;
        }

        //1. 获取需要删除的评论id
        String commentId = req.getParameter("cid");
        String blogId = req.getParameter("bid");
        if(commentId == null || "".equals(commentId)){
            req.getSession().setAttribute("comment","需要删除的评论不存在");
            resp.sendRedirect("blogDetail?id="+blogId);
            return;
        }

        //2. 在数据库中查找该评论是否存在
        CommentDao commentDao = new CommentDao();
        if(!commentDao.findCommentById(Integer.parseInt(commentId),null)){
            req.getSession().setAttribute("comment","需要删除的评论不存在");
            resp.sendRedirect("blogDetail?id="+blogId);
            return;
        }

        //3. 判断该评论和该用户是否匹配
        boolean active = commentDao.findCommentById(Integer.parseInt(commentId),user.getUsername());
        if(!active){
            req.getSession().setAttribute("comment","你不能删除别人的评论");
            resp.sendRedirect("blogDetail?id="+blogId);
            return;
        }

        //4. 此时可以正常删除评论
        int ret = commentDao.deleteComment(Integer.parseInt(commentId));
        if(ret == 1){
            resp.sendRedirect("blogDetail?id=" + blogId);
        }
    }
}
