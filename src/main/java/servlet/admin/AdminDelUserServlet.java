package servlet.admin;

import dao.UserDao;
import entity.User;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created with IntelliJ IDEA.
 * Description:管理员删除用户
 * User: ZhangYufei
 * Date: 2021-07-28
 * Time: 14:37
 */
@WebServlet("/adminDelUser")
public class AdminDelUserServlet extends HttpServlet {
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.setCharacterEncoding("utf-8");
        resp.setContentType("application/json;charset=utf-8");

        //0. 判断该用户是否为管理员
        User user = (User) req.getSession().getAttribute("user");
        if(user == null){
            //System.out.println("管理员未登录");
            resp.sendError(404,"管理员未登录");
            return;
        }
        if(!"admin".equals(user.getUsername())){
            //System.out.println("该用户非管理员");
            resp.sendError(404,"该用户非管理员");
            return;
        }

        //1. 获取需要删除的用户
        String username = req.getParameter("username");
        if(username == null){
            //System.out.println("没有需要删除的用户");
            resp.sendError(404,"没有需要删除的用户");
            return;
        }

        //2. 判断该用户在数据库中是否存在
        UserDao userDao = new UserDao();
        User haveUser = userDao.selectByName(username);
        if(haveUser == null){
            //System.out.println("该用户不存在");
            resp.sendError(404,"该用户不存在");
            return;
        }

        //3. 此时管理员进行删除用户操作
        int ret = userDao.deleteUser(username);
        if (ret == 1) {
            System.out.println("删除成功");
        }else{
            System.out.println("删除失败");
        }
    }
}
