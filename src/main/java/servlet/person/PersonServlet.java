package servlet.person;

import entity.User;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.WebContext;
import service.LoginService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: ZhangYufei
 * Date: 2021-08-01
 * Time: 22:24
 */
@WebServlet("/person")
public class PersonServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.setCharacterEncoding("utf-8");
        resp.setContentType("application/json;charset=utf-8");
        LoginService loginService = new LoginService();
        User user = loginService.checkLogin(req);
        if (user == null) {
            resp.sendRedirect("login");
            return;
        }
        boolean isLogin = true;
        String username = user.getUsername();
        int age = user.getAge();
        String gender = user.getGender();
        String email = user.getEmail();

        WebContext webContext = new WebContext(req, resp, getServletContext());
        //2) setVariable可以设置多个键值对，取决于模板代码怎么写
        // 模板中的每个 ${} 里面的内容都需要在 webContext 设定进去
        webContext.setVariable("isLogin", isLogin);
        webContext.setVariable("username", username);
        webContext.setVariable("age", age);
        webContext.setVariable("gender", gender);
        webContext.setVariable("email", email);
        //3) 进行渲染
        TemplateEngine engine = (TemplateEngine) getServletContext().getAttribute("engine");
        String html = engine.process("person", webContext);

        resp.setContentType("text/html;charset=utf8");
        resp.getWriter().write(html);
    }
}
