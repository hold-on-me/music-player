package servlet.find;

import dao.LoveMVDao;
import dao.LoveMusicDao;
import entity.MV;
import entity.Music;
import entity.User;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.WebContext;
import service.LoginService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: ZhangYufei
 * Date: 2021-08-01
 * Time: 14:45
 */
@WebServlet("/loveMvPage")
public class FindLoveMvServlet extends HttpServlet {
    private String getAndPost(HttpServletRequest req, HttpServletResponse resp){
        //0. 判断用户是否登录
        LoginService loginService = new LoginService();
        boolean isLogin = false;
        User user = loginService.checkLogin(req);
        if(user == null){
            return null;
        }
        isLogin = true;

        //1. 判断用户是否传入参数进行模糊查询，如果没有参数则查询全部喜欢的音乐
        String key = req.getParameter("key");
        LoveMVDao loveMvcDao = new LoveMVDao();
        List<MV> list = list = loveMvcDao.findLoveMv(key,user.getId());

        //2.构造博客页面
        //1) 通过 thymeleaf 进行渲染，渲染的时候需要定义一个"数据集合"的概念
        WebContext webContext = new WebContext(req, resp, getServletContext());
        //2) setVariable可以设置多个键值对，取决于模板代码怎么写
        // 模板中的每个 ${} 里面的内容都需要在 webContext 设定进去
        webContext.setVariable("list", list);
        webContext.setVariable("isLogin", isLogin);
        //3) 进行渲染
        TemplateEngine engine = (TemplateEngine) getServletContext().getAttribute("engine");
        return engine.process("loveMVPage", webContext);
    }
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.setCharacterEncoding("utf-8");
        resp.setContentType("application/json;charset=utf-8");

        String html = getAndPost(req,resp);
        if(html == null){
            resp.sendRedirect("login");
        }else {
            resp.setContentType("text/html;charset=utf8");
            resp.getWriter().write(html);
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.setCharacterEncoding("utf-8");
        resp.setContentType("application/json;charset=utf-8");

        String html = getAndPost(req,resp);
        if(html == null){
            resp.sendRedirect("login");
        }else {
            resp.setContentType("text/html;charset=utf8");
            resp.getWriter().write(html);
        }
    }
}
